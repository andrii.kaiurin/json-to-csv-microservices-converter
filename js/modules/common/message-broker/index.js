import { BullBrokerAdapter } from "./adapters/bull.adapter.js";
import { RabbitBrokerAdapter } from "./adapters/rabbit.adapter.js";
import { SQSBrokerAdapter } from "./adapters/sqs.adapter.js";
import { appConfig } from "../config/app.config.js";

export class MessageBrokerService {
  messageBroker;

  constructor() {
    if (appConfig.redisUrl) {
      this.messageBroker = new BullBrokerAdapter();
      return;
    }

    if (appConfig.rabbitUrl) {
      this.messageBroker = new RabbitBrokerAdapter();
      return;
    }

    this.messageBroker = new SQSBrokerAdapter();
  }

  async connect() {
    await this.messageBroker.connect();
  }

  async publishMessage(queue, message) {
    await this.messageBroker.publishMessage(queue, message);
  }

  async subscribeOnQueue(queue, handler) {
    await this.messageBroker.subscribeOnQueue(queue, handler);
  }

  async gracefulShutdown() {
    await this.messageBroker.gracefulShutdown();
  }
}
