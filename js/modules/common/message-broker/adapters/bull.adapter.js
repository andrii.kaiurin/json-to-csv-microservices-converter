import { Queue, Worker } from 'bullmq';
import { appConfig } from "../../config/app.config.js";

export class BullBrokerAdapter {
  connection;

  #host;
  #port;
  #queues = {};
  #workers = {}
  #defaultJobOptions = {
    removeOnComplete: true,
    removeOnFail: 1000
  }

  constructor() {
    this.#host = appConfig.redisUrl;
    this.#port = appConfig.redisPort;
  }

  async connect() {
    this.connection = {
      host: this.#host,
      port: this.#port
    }
  }

  #createQueue(queue) {
    this.#queues[queue] = new Queue(queue, {
      connection: this.connection,
      defaultJobOptions: this.#defaultJobOptions,
    });
  }

  async publishMessage(queue, message) {
    if (!this.#queues[queue]) {
      this.#createQueue(queue);
    }

    await this.#queues[queue].add('convertItem', message); // lack of design for job name
  }

  async subscribeOnQueue(queue, handler) {
    this.#workers[queue] = new Worker(queue, async (job) => {
      console.log('Received job message: ', job);

      await handler(job.data, job.id);
    }, {
      connection: this.connection,
    });
  }

  async gracefulShutdown() {
    for (const worker in this.#workers) {
      await worker.close();
    }
  }
}
