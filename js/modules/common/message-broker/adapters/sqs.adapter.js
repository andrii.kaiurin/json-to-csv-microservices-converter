import { Consumer } from 'sqs-consumer';
import { fromEnv } from "@aws-sdk/credential-providers";
import { SQSClient, SendMessageCommand } from "@aws-sdk/client-sqs";
import { appConfig } from "../../config/app.config.js";

import * as uuid from "uuid";

export class SQSBrokerAdapter {
  connection;

  #brokerUrl;

  #consumers = {};

  constructor() {
    this.#brokerUrl = appConfig.sqsUrl;
  }

  async connect() {
    this.connection = new SQSClient({
      region: 'eu-north-1',
      credentials: fromEnv()
    });
  }

  async publishMessage(queue, message) {
    const input = {
      QueueUrl: `${this.#brokerUrl}/${queue}.fifo`,
      MessageBody: message,
      DelaySeconds: Number("int"),
      MessageGroupId: 'ConverterTask',
      MessageDeduplicationId: uuid.v4()
    };

    console.log('input', input);

    const command = new SendMessageCommand(input);

    await this.connection.send(command);
  }

  subscribeOnQueue(queue, handler) {
    // not an abstract solution :(
    this.#consumers[queue] = Consumer.create({
      queueUrl: `${this.#brokerUrl}/${queue}.fifo`,
      handleMessage: async (message) => {
        console.log('Received new message: ', message);

        handler(message.Body, message.MessageId)
      },
      sqs: this.connection
    });

    this.#consumers[queue].on('error', (err) => {
      console.error('error', err.message);
    });

    this.#consumers[queue].on('processing_error', (err) => {
      console.error('processing_error', err.message);
    });

    this.#consumers[queue].start();
  }

  async gracefulShutdown() {
    for (const consumer in this.#consumers) {
      consumer.stop();
    }
  }
}
