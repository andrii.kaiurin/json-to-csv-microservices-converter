import amqplib from "amqplib";
import { appConfig } from "../../config/app.config.js";
import * as uuid from "uuid";

export class RabbitBrokerAdapter {
  connection;

  #brokerUrl;

  #queues = {};
  #consumers = {};

  #channel;

  constructor() {
    this.#brokerUrl = `amqp://${appConfig.rabbitUrl}:${appConfig.rabbitPort}`;
  }

  async connect() {
    this.connection = await amqplib.connect(`${this.#brokerUrl}`);

    this.#channel = await this.connection.createChannel();
  }

  async #assertQueue(queue) {
    this.#queues[queue] = await this.#channel.assertQueue(queue);
  }

  async publishMessage(queue, message) {
    if (!this.#queues[queue]) {
      await this.#assertQueue(queue);
    }

    this.#channel.sendToQueue(queue, Buffer.from(message), {
      persistent: true
    });
  }

  async subscribeOnQueue(queue, handler) {
    const consumerTag = uuid.v4();

    // not abstract solution for handler :(
    this.#consumers[queue] = await this.#channel.consume(queue, async (msg) => {
      const content = msg.content.toString();

      console.log('Received new message: ', content);

      await handler(content, msg.fields.deliveryTag)
    }, { noAck: true, consumerTag });
  }

  async gracefulShutdown() {
    for (const consumer in this.#consumers) {
      this.#channel.cancel(consumer.consumerTag);
    }
  }
}