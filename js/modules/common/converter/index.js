import { json2csv } from 'json-2-csv';
import { readFile, writeFile } from 'fs/promises';

const RESULT_FILE_PATH = './converted-files';
const UPLOADS_FILE_PATH = './uploads';

export const convertJsonToCsv = async (fileName, jobId) => {
  try {
    const pathToFile = `${UPLOADS_FILE_PATH}/${fileName}`;

    console.log('pathToFile - ', pathToFile);

    const jsonFileBuffer = await readFile(pathToFile, 'utf-8');

    const jsonData = JSON.parse(jsonFileBuffer);

    const csvString = await json2csv(jsonData);

    const resultFileName = fileName.split('.')[0];

    await writeFile(`${RESULT_FILE_PATH}/${resultFileName}-${jobId}.csv`, csvString);
  } catch (e) {
    console.log('Error on converting: ', e);
  }
}
