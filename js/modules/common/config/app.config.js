import 'dotenv/config';

export const appConfig = {
  rabbitUrl: process.env.RABBIT_URL,
  rabbitPort: process.env.RABBIT_PORT,

  redisUrl: process.env.REDIS_URL,
  redisPort: process.env.REDIS_PORT,

  sqsUrl: process.env.SQS_URL,

  converterTaskQueue: 'ConverterTaskQueue',
  converterLogQueue: 'ConverterLogQueue',
};
