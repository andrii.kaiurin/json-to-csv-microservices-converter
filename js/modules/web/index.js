import 'dotenv/config';
import Fastify from 'fastify';
import multipart from '@fastify/multipart';
import fs from 'fs';

import { pipeline } from 'stream/promises';

import * as uuid from 'uuid';
import { MessageBrokerService } from "../common/message-broker/index.js";
import { appConfig } from "../common/config/app.config.js";

const messageBrokerService = new MessageBrokerService();

const PATH_TO_UPLOADS = './uploads';

const fastify = Fastify({
  logger: true
});

fastify.register(multipart);

fastify.post('/create-job', async (request, reply) => {
  const dataFile = await request.file();

  const randomId = uuid.v4();
  const fileName = `${randomId}-${dataFile.filename}`;

  console.log(dataFile.filename);

  const writableStream = fs.createWriteStream(`${PATH_TO_UPLOADS}/${fileName}`, { 'flags': 'a' });

  await pipeline(dataFile.file, writableStream);

  await messageBrokerService.publishMessage(appConfig.converterTaskQueue, fileName);

  return { message: 'Job created!' };
})

try {
  await messageBrokerService.connect();

  await fastify.listen({ port: 3000 });
} catch (err) {
  fastify.log.error(err)
  process.exit(1)
}
