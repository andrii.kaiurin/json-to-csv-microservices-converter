import 'dotenv/config';

import { convertJsonToCsv } from "../common/converter/index.js";
import { MessageBrokerService } from "../common/message-broker/index.js";
import { appConfig } from "../common/config/app.config.js";

try {
  const messageBrokerService = new MessageBrokerService();

  await messageBrokerService.connect();

  await messageBrokerService.subscribeOnQueue(appConfig.converterTaskQueue, convertJsonToCsv);
} catch (error) {
  console.error('Worker error: ', error);
}
